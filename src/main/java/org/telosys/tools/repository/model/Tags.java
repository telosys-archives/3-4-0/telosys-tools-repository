/**
 *  Copyright (C) 2008-2017  Telosys project org. ( http://www.telosys.org/ )
 *
 *  Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.gnu.org/licenses/lgpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.telosys.tools.repository.model;

import java.io.Serializable;

import org.telosys.tools.generic.model.TagContainer;

public class Tags implements TagContainer, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final String VOID_STRING  = "" ;

	public Tags() {
		super();
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public boolean containsTag(String tagName) {
		return false;
	}
	
	@Override
	public String getTagValue(String tagName) {
		return VOID_STRING;
	}

	@Override
	public String getTagValue(String tagName, String defaultValue) {
		return defaultValue;
	}

	@Override
	public int getTagValueAsInt(String tagName, int defaultValue) {
		return defaultValue;
	}

	@Override
	public boolean getTagValueAsBoolean(String tagName, boolean defaultValue) {
		return defaultValue;
	}

}
